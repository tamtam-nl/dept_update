<?php

namespace Drupal\dept_update\Client;

use Drupal\Core\Site\Settings;

class Credentials
{
    private $settings;

    const TOKEN_INDEX = 'dept_update.token';
    const IS_PROD_INDEX = 'dept_update.is_prod';

    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->settings->get(self::TOKEN_INDEX);
    }

    public function isProd()
    {
        return $this->settings->get(self::IS_PROD_INDEX);
    }

}